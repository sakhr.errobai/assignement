package ma.octo.assignement.service;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.VirementNonExistantException;

import java.util.List;

public interface IVirementService {
    List<VirementDto> getAllVirements();
    VirementDto getVirement(Long id) throws VirementNonExistantException;
    VirementDto createTransaction(VirementDto virementDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException;
}
