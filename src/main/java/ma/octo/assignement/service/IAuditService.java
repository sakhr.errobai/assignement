package ma.octo.assignement.service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;

public interface IAuditService {
    Audit addAudit(String message, EventType auditType);
}
