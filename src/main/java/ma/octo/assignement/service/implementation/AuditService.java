package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.IAuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditService implements IAuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    private final AuditRepository auditRepository;

    public AuditService(AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    public Audit addAudit(String message, EventType auditType) {
        LOGGER.info("Audit de l'événement {}", auditType);
        LOGGER.info(message);

        Audit audit = new Audit();
        audit.setEventType(auditType);
        audit.setMessage(message);
        return auditRepository.save(audit);
    }

}
