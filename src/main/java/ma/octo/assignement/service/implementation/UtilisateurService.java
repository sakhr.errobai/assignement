package ma.octo.assignement.service.implementation;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UtilisateurService implements IUtilisateurService {

    private final UtilisateurRepository utilisateurRepository;

    @Override
    public List<UtilisateurDto> getAllUtilisateurs() {
        return UtilisateurMapper.mapToListDto(utilisateurRepository.findAll());
    }

    @Override
    public Utilisateur addUtilisateur(Utilisateur utilisateur) {
        return utilisateurRepository.save(utilisateur);
    }

    @Override
    public UtilisateurDto getUtilisateurByUsername(String username) throws Exception {
        Utilisateur utilisateur = utilisateurRepository.findUtilisateurByUsername(username);
        if(utilisateur == null) {
            throw new Exception("Utilisateur Not fuound");
        }

        return UtilisateurMapper.mapToDto(utilisateur);
    }

    @Override
    public boolean existsByUsername(String username) {
        return utilisateurRepository.existsUtilisateurByUsername(username);
    }
}
