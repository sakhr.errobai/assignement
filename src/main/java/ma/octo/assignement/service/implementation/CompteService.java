package ma.octo.assignement.service.implementation;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class CompteService implements ICompteService {

    private final CompteRepository compteRepository;

    public List<CompteDto> getAllComptes() {
        return CompteMapper.mapToListDto(compteRepository.findAll());
    }

    public Compte getCompte(String nrCompte) throws CompteNonExistantException {
        Compte compte = compteRepository.findByNrCompte(nrCompte);
        if( compte == null ) {
            throw new CompteNonExistantException("Compte Non existant");
        }

        return compte;
    }

    @Override
    public Compte addCompte(Compte compte) {
        return compteRepository.save(compte);
    }

    @Override
    public Compte getCompteByRib(String rib) throws CompteNonExistantException {
        Compte compte = compteRepository.findByRib(rib);
        if(compte == null) {
            throw new CompteNonExistantException("Compte Non existant");
        }
        return compte;
    }

}
