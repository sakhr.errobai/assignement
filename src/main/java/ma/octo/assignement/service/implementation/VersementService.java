package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.VersementNonExistantException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IVersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VersementService implements IVersementService {

    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIMAL = 10;

    Logger LOGGER = LoggerFactory.getLogger(VersementService.class);
    private final VersementRepository versementRepository;
    private final ICompteService compteService;
    private final IAuditService auditService;

    public VersementService(VersementRepository versementRepository, ICompteService compteService, IAuditService auditService) {
        this.versementRepository = versementRepository;
        this.compteService = compteService;
        this.auditService = auditService;
    }

    @Override
    public VersementDto getVersement(Long id) throws VersementNonExistantException {
        return VersementMapper.mapToDto(versementRepository.findById(id)
                .orElseThrow(()->new VersementNonExistantException("Versement Non existant"))
        );
    }

    @Override
    public List<VersementDto> getAllVersements() {
        return VersementMapper.mapToListDto(versementRepository.findAll());
    }

    @Override
    public VersementDto createTransaction(VersementDto versementDto)
            throws CompteNonExistantException, TransactionException {
        Compte compteBeneficiaire = compteService.getCompteByRib(versementDto.getRibCompteBeneficiaire());
        if(versementDto.getMontantVersement() == null ||
            versementDto.getMontantVersement().compareTo(BigDecimal.ZERO) == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if(versementDto.getMontantVersement().intValue() < MONTANT_MINIMAL ) {
            LOGGER.error("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(versementDto.getMontantVersement()));

        Versement versement = VersementMapper.mapFromDto(versementDto);
        versement.setCompteBeneficiaire(compteBeneficiaire);

        String message = "Versement par " + versement.getNomPrenomEmetteur() + " vers " + versement.getCompteBeneficiaire().getRib() +
                " d'un montant de " + versement.getMontantVersement() + "DH";
        auditService.addAudit(message, EventType.VERSEMENT);
        return VersementMapper.mapToDto(versementRepository.save(versement));
    }
}
