package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.VirementNonExistantException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IVirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class VirementService implements IVirementService {

    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);
    private final VirementRepository virementRepository;
    private final ICompteService compteService;
    private final IAuditService auditService;

    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIMAL = 10;

    public VirementService(VirementRepository virementRepository, ICompteService compteService, IAuditService auditService) {
        this.virementRepository = virementRepository;
        this.compteService = compteService;
        this.auditService = auditService;
    }

    @Override
    public List<VirementDto> getAllVirements() {
        return VirementMapper.mapToListDto(virementRepository.findAll());
    }

    @Override
    public VirementDto getVirement(Long id) throws VirementNonExistantException {
        return VirementMapper.mapToDto(virementRepository.findById(id)
                                                         .orElseThrow(()->new VirementNonExistantException("Virement non existant"))
                );
    }

    @Override
    public VirementDto createTransaction(VirementDto virementDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        Compte compteEmetteur = compteService.getCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteService.getCompte(virementDto.getNrCompteBeneficiaire());

        if(virementDto.getMontantVirement() == null ||
                virementDto.getMontantVirement().compareTo(BigDecimal.ZERO) == 0 ) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if(virementDto.getMontantVirement().intValue() < MONTANT_MINIMAL) {
            LOGGER.error("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if(compteEmetteur.getSolde().compareTo(virementDto.getMontantVirement()) == -1) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(virementDto.getMontantVirement()));

        Virement virement = VirementMapper.mapFromDto(virementDto);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setMotifVirement(virementDto.getMotif());
        virement.setMontantVirement(virementDto.getMontantVirement());


        String auditMessage =  "Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString() + "DH";

        auditService.addAudit(auditMessage, EventType.VIREMENT);

        return VirementMapper.mapToDto(virementRepository.save(virement));
    }

}
