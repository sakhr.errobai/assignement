package ma.octo.assignement.service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.VersementNonExistantException;

import java.util.List;

public interface IVersementService {
    VersementDto getVersement(Long id) throws VersementNonExistantException;
    List<VersementDto> getAllVersements();
    VersementDto createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException;
}
