package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;

import java.util.List;

public interface IUtilisateurService {
    List<UtilisateurDto> getAllUtilisateurs();
    Utilisateur addUtilisateur(Utilisateur utilisateur);
    UtilisateurDto getUtilisateurByUsername(String username) throws Exception;
    boolean existsByUsername(String username);
}
