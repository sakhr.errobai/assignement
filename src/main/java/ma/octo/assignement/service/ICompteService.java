package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;

import java.util.List;

public interface ICompteService {
    List<CompteDto> getAllComptes();
    Compte getCompte(String nrCompte) throws CompteNonExistantException;
    Compte addCompte(Compte compte) throws Exception;
    Compte getCompteByRib(String rib) throws CompteNonExistantException;
}
