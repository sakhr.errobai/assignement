package ma.octo.assignement.security.service;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.service.implementation.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UtilisateurService utilisateurService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UtilisateurDto user;
        try {
            user = utilisateurService.getUtilisateurByUsername(username);
        } catch (Exception e) {
            throw new UsernameNotFoundException("User Not Found with email: " + username);
        }

        return UserDetailsImpl.build(user);
    }
}
