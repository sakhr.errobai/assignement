package ma.octo.assignement.security.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ma.octo.assignement.dto.UtilisateurDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class UserDetailsImpl implements UserDetails {

    private final Long id;
    private final String username;

    @JsonIgnore
    private final String password;

    private final Boolean enabled;

    public Long getId() {
        return id;
    }

    private final Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(Long id, String username, String password,
                           Collection<? extends GrantedAuthority> authorities, boolean enabled) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.enabled = enabled;
    }

    public static UserDetailsImpl build(UtilisateurDto user) {
        List<GrantedAuthority> authorities = new ArrayList<>();

        return new UserDetailsImpl(
                null,
                user.getUsername(),
                user.getPassword(),
                authorities,
                true
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null || this.getClass() != obj.getClass() )
            return false;

        UserDetailsImpl user = (UserDetailsImpl)  obj;

        return Objects.equals(id, user.id);

    }

}
