package ma.octo.assignement.web;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.VirementNonExistantException;
import ma.octo.assignement.service.IVirementService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/virements")
class VirementController {

    private final IVirementService virementService;

    public VirementController(IVirementService virementService) {
        this.virementService = virementService;
    }

    @GetMapping
    public List<VirementDto> getAllVirements() {
        return virementService.getAllVirements();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public VirementDto createTransaction(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        return virementService.createTransaction(virementDto);
    }

    @GetMapping("/{id}")
    public VirementDto getVirement(@PathVariable Long id) throws VirementNonExistantException {
        return virementService.getVirement(id);
    }

}
