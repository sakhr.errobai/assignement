package ma.octo.assignement.web;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.VersementNonExistantException;
import ma.octo.assignement.service.IVersementService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/versements")
public class VersementController {

    private final IVersementService versementService;

    public VersementController(IVersementService versementService) {
        this.versementService = versementService;
    }

    @GetMapping("/{id}")
    public VersementDto getVesement(@PathVariable Long id) throws VersementNonExistantException {
        return versementService.getVersement(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public VersementDto createTransaction(@RequestBody VersementDto versementDto) throws TransactionException, CompteNonExistantException {
        return versementService.createTransaction(versementDto);
    }

    @GetMapping
    public List<VersementDto> getAllVersements() {
        return versementService.getAllVersements();
    }

}
