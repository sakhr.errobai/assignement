package ma.octo.assignement.web;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.payload.JwtResponse;
import ma.octo.assignement.payload.LoginRequest;
import ma.octo.assignement.security.jwt.JwtUtils;
import ma.octo.assignement.security.service.UserDetailsImpl;
import ma.octo.assignement.service.implementation.UtilisateurService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/authentification")
public class AuthentificationController {

    private final UtilisateurService utilisateurService;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final PasswordEncoder encoder;



    public AuthentificationController(UtilisateurService utilisateurService, AuthenticationManager authenticationManager, JwtUtils jwtUtils, PasswordEncoder encoder) {
        this.utilisateurService = utilisateurService;
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
        this.encoder = encoder;
    }

    @PostMapping("/login")
    public JwtResponse authentificationUtilisateur(@RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        return new JwtResponse(jwt, userDetails.getUsername());
    }

    @PostMapping("/inscription")
    public ResponseEntity<?> inscriptionUtilisateur(@RequestBody UtilisateurDto utilisateurDto) {
        if(utilisateurService.existsByUsername(utilisateurDto.getUsername())) {
            return ResponseEntity.badRequest()
                    .body("Erreur: Username existe deja!");
        }
        utilisateurDto.setPassword(encoder.encode(utilisateurDto.getPassword()));
        utilisateurService.addUtilisateur(UtilisateurMapper.mapFromDto(utilisateurDto));
        return ResponseEntity.ok("Inscription avec succes!");
    }

}
