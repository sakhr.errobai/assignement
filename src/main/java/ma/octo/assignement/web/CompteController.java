package ma.octo.assignement.web;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.service.ICompteService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/comptes")
public class CompteController {
    private final ICompteService compteService;

    public CompteController(ICompteService compteService) {
        this.compteService = compteService;
    }

    @GetMapping
    public List<CompteDto> getAllComptes() {
        return compteService.getAllComptes();
    }
}
