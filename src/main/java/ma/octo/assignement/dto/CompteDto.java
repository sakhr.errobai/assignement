package ma.octo.assignement.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CompteDto {
    private String nrCompte;
    private String rib;
    private BigDecimal solde;
    private String username;
}
