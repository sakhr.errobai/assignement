package ma.octo.assignement.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UtilisateurDto {
    private String username;
    private String gender;
    private String lastname;
    private String firstname;
    private Date birthDate;
    private String password;
}
