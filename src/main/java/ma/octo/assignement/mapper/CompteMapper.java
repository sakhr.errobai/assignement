package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.CompteDto;

import java.util.List;
import java.util.stream.Collectors;

public class CompteMapper {

    public static CompteDto mapToDto(Compte compte) {
        CompteDto compteDto = new CompteDto();
        compteDto.setNrCompte(compte.getNrCompte());
        compteDto.setRib(compte.getRib());
        compteDto.setSolde(compte.getSolde());
        compteDto.setUsername(compte.getUtilisateur().getUsername());

        return compteDto;
    }

    public static Compte mapFromDto(CompteDto compteDto) {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUsername(compteDto.getUsername());

        Compte compte = new Compte();
        compte.setNrCompte(compteDto.getNrCompte());
        compte.setRib(compteDto.getRib());
        compte.setSolde(compteDto.getSolde());
        compte.setUtilisateur(utilisateur);

        return compte;
    }

    public static List<CompteDto> mapToListDto(List<Compte> comptes) {
        return comptes
                .stream()
                .map(CompteMapper::mapToDto)
                .collect(Collectors.toList());
    }

}
