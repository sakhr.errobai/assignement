package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

import java.util.List;
import java.util.stream.Collectors;

public class VersementMapper {
    
    public static VersementDto mapToDto(Versement versement) {
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(versement.getMontantVersement());
        versementDto.setNomPrenomEmetteur(versement.getNomPrenomEmetteur());
        versementDto.setRibCompteBeneficiaire(versement.getCompteBeneficiaire().getRib());
        versementDto.setMotifVersement(versement.getMotifVersement());
        versementDto.setDateExecution(versement.getDateExecution());
        
        return versementDto;
    }

    public static Versement mapFromDto(VersementDto versementDto) {
        Compte compteBeneficiaire = new Compte();
        compteBeneficiaire.setRib(versementDto.getRibCompteBeneficiaire());

        Versement versement = new Versement();
        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setNomPrenomEmetteur(versementDto.getNomPrenomEmetteur());
        versement.setCompteBeneficiaire(compteBeneficiaire);
        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setDateExecution(versementDto.getDateExecution());

        return versement;
    }

    public static List<VersementDto> mapToListDto(List<Versement> versements) {
        return versements
                .stream()
                .map(VersementMapper::mapToDto)
                .collect(Collectors.toList());
    }
    
}
