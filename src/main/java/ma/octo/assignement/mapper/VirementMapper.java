package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

import java.util.List;
import java.util.stream.Collectors;

public class VirementMapper {

    public static VirementDto mapToDto(Virement virement) {
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());
        virementDto.setMontantVirement(virement.getMontantVirement());

        return virementDto;
    }

    public static Virement mapFromDto(VirementDto virementDto) {
        Compte compteBenefiaire = new Compte();
        compteBenefiaire.setNrCompte(virementDto.getNrCompteBeneficiaire());

        Compte compteEmetteur = new Compte();
        compteEmetteur.setNrCompte(virementDto.getNrCompteEmetteur());

        Virement virement = new Virement();
        virement.setCompteEmetteur(compteEmetteur);
        virement.setCompteBeneficiaire(compteBenefiaire);
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setMotifVirement(virementDto.getMotif());
        virement.setDateExecution(virementDto.getDate());

        return virement;
    }

    public static List<VirementDto> mapToListDto(List<Virement> virements) {
        return virements
                .stream()
                .map(VirementMapper::mapToDto)
                .collect(Collectors.toList());
    }
    
}
