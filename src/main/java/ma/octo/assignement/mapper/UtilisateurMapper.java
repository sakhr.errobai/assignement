package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;

import java.util.List;
import java.util.stream.Collectors;

public class UtilisateurMapper {

    public static UtilisateurDto mapToDto(Utilisateur utilisateur) {
        UtilisateurDto utilisateurDto = new UtilisateurDto();
        utilisateurDto.setUsername(utilisateur.getUsername());
        utilisateurDto.setFirstname(utilisateur.getFirstname());
        utilisateurDto.setLastname(utilisateur.getLastname());
        utilisateurDto.setGender(utilisateur.getGender());
        utilisateurDto.setBirthDate(utilisateur.getBirthdate());
        utilisateurDto.setPassword(utilisateur.getPassword());

        return utilisateurDto;
    }

    public static Utilisateur mapFromDto(UtilisateurDto utilisateurDto) {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUsername(utilisateurDto.getUsername());
        utilisateur.setFirstname(utilisateurDto.getFirstname());
        utilisateur.setLastname(utilisateurDto.getLastname());
        utilisateur.setGender(utilisateurDto.getGender());
        utilisateur.setBirthdate(utilisateurDto.getBirthDate());
        utilisateur.setPassword(utilisateurDto.getPassword());

        return utilisateur;
    }

    public static List<UtilisateurDto> mapToListDto(List<Utilisateur> utilisateurs) {
        return utilisateurs
                .stream()
                .map(UtilisateurMapper::mapToDto)
                .collect(Collectors.toList());
    }

}
