package ma.octo.assignement.implementation;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.VersementNonExistantException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.implementation.AuditService;
import ma.octo.assignement.service.implementation.CompteService;
import ma.octo.assignement.service.implementation.VersementService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VersementServiceTest {

    @Mock
    private VersementRepository versementRepository;

    @Mock
    private CompteService compteService;

    @Mock
    private AuditService auditService;

    @InjectMocks
    private VersementService versementService;
    
    private final Date now = new Date();

    @Test
    void canGetVersement() {
        // given
        Long id = 1L;
        VersementDto versementDto = new VersementDto();
        versementDto.setNomPrenomEmetteur("Errobai Sakhr");
        versementDto.setMontantVersement(BigDecimal.TEN);
        versementDto.setMotifVersement("Motif");
        versementDto.setRibCompteBeneficiaire("RIB1");
        versementDto.setDateExecution(now);

        Versement versement = new Versement();
        versement.setNomPrenomEmetteur("Errobai Sakhr");
        versement.setMontantVersement(BigDecimal.TEN);
        versement.setMotifVersement("Motif");
        versement.setCompteBeneficiaire(new Compte());
        versement.setDateExecution(now);

        try(MockedStatic<VersementMapper> mockStatic = Mockito.mockStatic(VersementMapper.class)) {
            //when
            mockStatic.when((MockedStatic.Verification) VersementMapper.mapToDto(versement)).thenReturn(versementDto);
            when(versementRepository.findById(id)).thenReturn(Optional.of(versement));

            VersementDto result = versementService.getVersement(id);

            //then
            assertEquals("Errobai Sakhr", result.getNomPrenomEmetteur());
            assertEquals(BigDecimal.TEN, result.getMontantVersement());
            assertEquals("Motif", result.getMotifVersement());
            assertEquals("RIB1", result.getRibCompteBeneficiaire());
            assertEquals(now, result.getDateExecution());

        } catch (VersementNonExistantException e) {
            e.printStackTrace();
        }
    }

    @Test
    void getVersementWillThrowExceptionWhenIdNotFound() {
        // given
        Long id = 1L;
        VersementDto versementDto = new VersementDto();
        versementDto.setNomPrenomEmetteur("Errobai Sakhr");
        versementDto.setMontantVersement(BigDecimal.TEN);
        versementDto.setMotifVersement("Motif");
        versementDto.setRibCompteBeneficiaire("RIB1");
        versementDto.setDateExecution(now);

        Versement versement = new Versement();
        versement.setNomPrenomEmetteur("Errobai Sakhr");
        versement.setMontantVersement(BigDecimal.TEN);
        versement.setMotifVersement("Motif");
        versement.setCompteBeneficiaire(new Compte());
        versement.setDateExecution(now);

        try(MockedStatic<VersementMapper> mockStatic = Mockito.mockStatic(VersementMapper.class)) {
            //when
            mockStatic.when((MockedStatic.Verification) VersementMapper.mapToDto(versement)).thenReturn(versementDto);
            when(versementRepository.findById(id)).thenReturn(Optional.empty());

            //then
            assertThatThrownBy( () -> versementService.getVersement(id))
                    .isInstanceOf(VersementNonExistantException.class);
        }
    }

    @Test
    @Disabled
    void getAllVersements() {
        //given
        List<VersementDto> versementDtoList = new ArrayList<>();
        List<Versement> versementList = new ArrayList<>();

        VersementDto versementDto = new VersementDto();
        versementDto.setNomPrenomEmetteur("Errobai Sakhr");
        versementDto.setMontantVersement(BigDecimal.TEN);
        versementDto.setMotifVersement("Motif");
        versementDto.setRibCompteBeneficiaire("RIB1");
        versementDto.setDateExecution(now);
        
        VersementDto versementDto2 = new VersementDto();
        versementDto2.setNomPrenomEmetteur("Errobai Sakhr");
        versementDto2.setMontantVersement(BigDecimal.TEN);
        versementDto2.setMotifVersement("Motif");
        versementDto2.setRibCompteBeneficiaire("RIB1");
        versementDto2.setDateExecution(now);

        versementDtoList.add(versementDto);
        versementDtoList.add(versementDto2);

        Versement versement = new Versement();
        versement.setNomPrenomEmetteur("Errobai Sakhr");
        versement.setMontantVersement(BigDecimal.TEN);
        versement.setMotifVersement("Motif");
        versement.setCompteBeneficiaire(new Compte());
        versement.setDateExecution(now);
        
        Versement versement2 = new Versement();
        versement2.setNomPrenomEmetteur("Errobai Sakhr");
        versement2.setMontantVersement(BigDecimal.TEN);
        versement2.setMotifVersement("Motif");
        versement2.setCompteBeneficiaire(new Compte());
        versement2.setDateExecution(now);

        versementList.add(versement);
        versementList.add(versement2);

        try(MockedStatic<VersementMapper> mockStatic = Mockito.mockStatic(VersementMapper.class)) {
            //when
            mockStatic.when((MockedStatic.Verification) VersementMapper.mapToListDto(versementList)).thenReturn(versementDtoList);
            when(versementRepository.findAll()).thenReturn(versementList);

            List<VersementDto> result = versementService.getAllVersements();

            //then
            for(int i = 0; i < result.size(); i++) {
                assertEquals(versementDtoList.get(i).getNomPrenomEmetteur(), result.get(i).getNomPrenomEmetteur());
                assertEquals(versementDtoList.get(i).getMontantVersement(), result.get(i).getMontantVersement());
                assertEquals(versementDtoList.get(i).getMotifVersement(), result.get(i).getMotifVersement());
                assertEquals(versementDtoList.get(i).getRibCompteBeneficiaire(), result.get(i).getRibCompteBeneficiaire());
                assertEquals(now, result.get(i).getDateExecution());
            }

        }

    }

    @Test
    void createTransactionWillThrowWhenMontantIsNull(){
        // given
        VersementDto versementDto = new VersementDto();
        versementDto.setNomPrenomEmetteur("Errobai Sakhr");
        versementDto.setMontantVersement(BigDecimal.ZERO);
        versementDto.setMotifVersement("Motif");
        versementDto.setRibCompteBeneficiaire("RIB1");
        versementDto.setDateExecution(now);

        // when
        // then
        assertThatThrownBy( () -> versementService.createTransaction(versementDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Montant vide");
    }

    @Test
    void createTransactionWillThrowWhenMontantIsGreaterThanMontantMax() {
        // given
        VersementDto versementDto = new VersementDto();
        versementDto.setNomPrenomEmetteur("Errobai Sakhr");
        versementDto.setMontantVersement(new BigDecimal("100001"));
        versementDto.setMotifVersement("Motif");
        versementDto.setRibCompteBeneficiaire("RIB1");
        versementDto.setDateExecution(now);

        // when
        // then
        assertThatThrownBy( () -> versementService.createTransaction(versementDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Montant maximal de virement dépassé");
    }

    @Test
    void createTransactionWillThrowWhenMontantIsLessThanMontantMin() {
        // given
        VersementDto versementDto = new VersementDto();
        versementDto.setNomPrenomEmetteur("Errobai Sakhr");
        versementDto.setMontantVersement(new BigDecimal("9"));
        versementDto.setMotifVersement("Motif");
        versementDto.setRibCompteBeneficiaire("RIB1");
        versementDto.setDateExecution(now);

        // when
        // then
        assertThatThrownBy( () -> versementService.createTransaction(versementDto))
                .isInstanceOf(TransactionException.class)
                .hasMessageContaining("Montant minimal de virement non atteint");
    }

    @Test
    void canGetVersementWhenVersementisAdded() {
        // given
        Long id = 1L;
        VersementDto versementDto = new VersementDto();
        versementDto.setNomPrenomEmetteur("Errobai Sakhr");
        versementDto.setMontantVersement(BigDecimal.TEN);
        versementDto.setMotifVersement("Motif");
        versementDto.setRibCompteBeneficiaire("RIB1");
        versementDto.setDateExecution(now);

        Versement versement = new Versement();
        versement.setNomPrenomEmetteur("Errobai Sakhr");
        versement.setMontantVersement(BigDecimal.TEN);
        versement.setMotifVersement("Motif");
        versement.setCompteBeneficiaire(new Compte());
        versement.setDateExecution(now);

        Compte compte = new Compte();
        compte.setNrCompte("010000B025001000");
        compte.setRib("RIB2");
        compte.setSolde(BigDecimal.valueOf(140000L));
        compte.setUtilisateur(new Utilisateur());

        try(MockedStatic<VersementMapper> mockStatic = Mockito.mockStatic(VersementMapper.class)) {
            //when
            mockStatic.when((MockedStatic.Verification) VersementMapper.mapToDto(versement)).thenReturn(versementDto);
            mockStatic.when((MockedStatic.Verification) VersementMapper.mapFromDto(versementDto)).thenReturn(versement);
            when(versementRepository.save(versement)).thenReturn(versement);
            when(compteService.getCompteByRib(anyString())).thenReturn(compte);
            VersementDto result = versementService.createTransaction(versementDto);

            //then
            assertEquals("Errobai Sakhr", result.getNomPrenomEmetteur());
            assertEquals(BigDecimal.TEN, result.getMontantVersement());
            assertEquals("Motif", result.getMotifVersement());
            assertEquals("RIB1", result.getRibCompteBeneficiaire());
            assertEquals(now, result.getDateExecution());

        } catch (TransactionException | CompteNonExistantException e) {
            e.printStackTrace();
        }


    }

}